package service;

import config.HibernateUtils;
import model.Account;
import model.Client;
import repository.AccountRepository;
import repository.ClientRepository;

import java.util.List;
import java.util.Scanner;

public class AccountService {

    private AccountRepository accountRepository;
    private Client client;
    private ClientService clientServiceVar;
    public Account account;

    public static final double EURO = 4.83;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void warningMessage() {
        System.out.println("You don`t have an account. Please create one!");
    }

    public boolean isAccountAvailable(ClientService clientService) {
        return accountRepository.findByDebitTypeAndCNP(clientService.getClientTemp().getCNP()) != null;
    }

    public void registerAccount(ClientService clientService) {
        ClientRepository clientRepository = new ClientRepository(HibernateUtils.getSessionFactory());
        if(!isAccountAvailable(clientService)) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("\nRegister your Account");
            System.out.print("Select your account type DEBIT : ");
            String type = scanner.nextLine();
            System.out.print("Select your currency LEI | EURO | DOLLARS: ");
            String currency = scanner.nextLine();
            System.out.print("Set your balance : ");
            double balance = scanner.nextDouble();

            clientServiceVar = clientService;
            client = clientRepository.findByUsername(clientService.getUsernameTemp());
            account = new Account(currency, balance, type, client);
            accountRepository.insert(account);
        }else {
            System.out.println("You already have an account");
        }
    }

    public void viewPortfolioAndBalance(ClientService clientService) {
        if (isAccountAvailable(clientService)) {
            List<Account> accountList = accountRepository.findByCNPList(clientService.getClientTemp().getCNP());
            for (Account account1 : accountList) {
                System.out.println("Balance : " + account1.getBalance());
                System.out.println("Currency : " + account1.getCurrency());
                System.out.println("Type : " + account1.getType());
                System.out.println("---------------------------");
            }
        } else {
            warningMessage();
        }
    }

    /**
     * find current account currency type
     *
     * @param clientService
     */
    public void transferMoney(ClientService clientService) {
        if (isAccountAvailable(clientService)) {
            Account differentPersonAccount;
            Scanner scanner = new Scanner(System.in);
            System.out.print("Type CNP of person : ");
            String CNP = scanner.nextLine();
            System.out.print("What is the amount you want to transfer : ");
            double amount = scanner.nextDouble();

            account = accountRepository.findByDebitTypeAndCNP(clientService.getClientTemp().getCNP());
            System.out.println(account);
            differentPersonAccount = accountRepository.findByDebitTypeAndCNP(CNP);

            //currency type of current account
            if (account.getCurrency().equals("LEI")) {
                if (differentPersonAccount == null) {
                    System.out.println("This person does`t have an account");
                } else {
                    if (differentPersonAccount.getCurrency().equals("LEI")) {
                        if (account.getBalance() >= amount) {
                            differentPersonAccount.setBalanceWithAmount(amount);
                            account.setBalance(account.getBalance() - amount);
                        } else {
                            System.out.println("You don`t have enough money in your account");
                        }
                    } else if (differentPersonAccount.getCurrency().equals("EURO")) {
                        if (account.getBalance() >= (amount / EURO)) {
                            differentPersonAccount.setBalanceWithAmount(amount / EURO);
                            account.setBalance(account.getBalance() - amount);
                        } else {
                            System.out.println("You don`t have enough money in your account");
                        }
                    }
                }
            } else if (account.getCurrency().equals("EURO")) {
                if (differentPersonAccount == null) {
                    System.out.println("This person does`t have an account");
                } else {
                    if (differentPersonAccount.getCurrency().equals("LEI")) {
                        if (account.getBalance() >= amount) {
                            differentPersonAccount.setBalanceWithAmount(amount * EURO);
                            account.setBalance(account.getBalance() - amount);
                        } else {
                            System.out.println("You don`t have enough money in your account");
                        }
                    } else if (differentPersonAccount.getCurrency().equals("EURO")) {
                        if (account.getBalance() >= amount) {
                            differentPersonAccount.setBalanceWithAmount(amount);
                            account.setBalance(account.getBalance() - amount);
                        } else {
                            System.out.println("You don`t have enough money in your account");
                        }
                    }
                }
            }

            accountRepository.update(differentPersonAccount);
            accountRepository.update(account);
        } else {
            warningMessage();
        }

    }

    public void depositCash(ClientService clientService) {
        if (isAccountAvailable(clientService)) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Input the amount you want to deposit : ");
            double amount = scanner.nextDouble();
            account.setBalance(account.getBalance() + amount);
            accountRepository.update(account);
        } else {
            warningMessage();
        }
    }

    //you can receive $amount of money into your debit account but only if your credit account is 0;
    //borrow money
    //you can have only one credit account
    public void createCreditAccount(ClientService clientService) {
        if (isAccountAvailable(clientService)) {
            ClientRepository clientRepository = new ClientRepository(HibernateUtils.getSessionFactory());
            Scanner scanner = new Scanner(System.in);
            client = clientRepository.findByUsername(clientService.getUsernameTemp());

            if (checkIfCreditAccountIsPresent(client)) {
                System.out.println("\nRegister your Credit Account");
                System.out.print("Select your currency LEI | EURO | DOLLARS: ");
                String currency = scanner.nextLine();
                System.out.print("Set the amount you want to borrow : ");
                double balance = scanner.nextDouble();

                account = new Account(currency, -balance, "CREDIT", client);
                accountRepository.insert(account);

                account = accountRepository.findByDebitTypeAndCNP(client.getCNP());
                account.setBalance(account.getBalance() + balance);
                accountRepository.update(account);
            } else {
                System.out.println("You already have an CREDIT ACCOUNT");
            }
        } else {
            warningMessage();
        }
    }

    public boolean checkIfCreditAccountIsPresent(Client client) {
        return accountRepository.findByCreditTypeAndCNP(client.getCNP()) == null;
    }

    public void viewDetailsAboutAccounts(ClientService clientService) {
        if (isAccountAvailable(clientService)) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("What is your username ? :");
            String username = scanner.nextLine();
            List<Account> accountList = accountRepository.findAllClientAccounts(username);
            accountList.forEach(System.out::println);
        } else {
            warningMessage();
        }
    }
}
