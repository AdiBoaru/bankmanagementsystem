package service;

import config.HibernateUtils;
import model.Bank;
import model.Client;
import repository.BankRepository;
import repository.ClientRepository;

import java.util.Scanner;

public class ClientService {
    ClientRepository clientRepository;
    private String usernameTemp;
    private Client clientTemp;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public void register() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nRegister your account");
        System.out.print("CNP :");
        String CNP = scanner.nextLine();
        System.out.print("First Name : ");
        String firstName = scanner.nextLine();
        System.out.print("Last Name : ");
        String lastName = scanner.nextLine();
        System.out.print("Email : ");
        String email = scanner.nextLine();
        System.out.print("Username : ");
        String username = scanner.nextLine();
        //verify if username is unique
        while (!userRegisterVerify(username)) {
            System.out.print("Username : ");
            username = scanner.nextLine();
        }
        System.out.print("Password : ");
        String password = scanner.nextLine();
        System.out.print("Bank name : ");
        String bankName = scanner.nextLine();
        System.out.println("\nRegistration successful !\n");


        Bank bank = new BankRepository(HibernateUtils.getSessionFactory()).findByName(bankName);
        setClientTemp(new Client(CNP, firstName, lastName, email, username, password, bank));
        clientRepository.insert(getClientTemp());
    }

    public boolean userRegisterVerify(String username) {
        if (clientRepository.findByUsername(username) == null) {
            return true;
        } else {
            System.out.println("Username is already in use");
        }
        return false;
    }

    public boolean isLoggedIn() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nUsername : ");
        String username = scanner.nextLine();
        System.out.print("Password : ");
        String password = scanner.nextLine();

        while (loginVerify(username, password)) {
            System.out.print("\nUsername : ");
            username = scanner.nextLine();
            System.out.print("Password : ");
            password = scanner.nextLine();
        }
        usernameTemp = username;
        setClientTemp(clientRepository.findByUsername(username));
        return true;
    }

    /**
     * bug, infinite while loop ->return the wrong condition
     */
    public boolean loginVerify(String username, String password) {
        if (clientRepository.findByUsername(username) == null) {
            System.out.println("Wrong Username");
            return true;
        }
        if (clientRepository.findByUsernamePass(username, password) != null) {
            // System.out.println(clientRepository.findByUsernamePass(username,password));
            System.out.println("Login Successful!");
            return false;
        } else {
            System.out.println("Wrong password");
            return true;
        }
    }

    public String getUsernameTemp() {
        return usernameTemp;
    }

    public Client getClientTemp() {
        return clientTemp;
    }

    public void setClientTemp(Client clientTemp) {
        this.clientTemp = clientTemp;
    }
}
