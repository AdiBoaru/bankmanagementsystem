package service;

import config.HibernateUtils;
import model.Bank;
import repository.BankRepository;

import java.util.Scanner;

public class BankService {

    private final BankRepository bankRepository;

    public BankService() {
        this.bankRepository = new BankRepository(HibernateUtils.getSessionFactory());
    }

    public boolean isBankAvailable(String name) {
        return bankRepository.findByName(name) == null;
    }

    public void registerBank() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Register the bank");
        System.out.print("Bank name : ");
        String name = scanner.nextLine();
        System.out.print("Bank address : ");
        String address = scanner.nextLine();

        if (isBankAvailable(name)) {
            bankRepository.insert(new Bank(name, address));
        }else {
            System.out.println("\nBank already exists!");
        }
    }

    public void selectAvailableBank() {
        System.out.println("Available banks in the system");
        int i = 1;
        for (Bank bank : bankRepository.findAll()) {
            System.out.print(i++ + " : Bank Name : " + bank.getName() + " --- Address : " + bank.getAddress() + "\n");
        }
    }

}
