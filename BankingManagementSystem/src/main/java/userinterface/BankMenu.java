package userinterface;

import config.HibernateUtils;
import repository.BankRepository;
import service.BankService;

import java.util.Scanner;

public class BankMenu {
    private final BankService bankService;
    private ClientMenu clientMenu;

    public BankMenu() {
        this.bankService = new BankService();
        this.clientMenu = new ClientMenu();
    }

    public void viewBankMenu() {
        System.out.println("\n\tWelcome to Bank Management System");
        System.out.println("1 - Register a bank");
        System.out.println("2 - View Banks");
        System.out.println("3 - Client Menu");
        System.out.println("4 - Exit\n");

    }

    public void runBank() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            viewBankMenu();
            switch (scanner.nextInt()) {
                case 1:
                    bankService.registerBank();
                    break;
                case 2:
                    bankService.selectAvailableBank();
                    break;
                case 3:
                    clientMenu.run();
                    break;
                case 4:
                    return;
            }

        }
    }
}
