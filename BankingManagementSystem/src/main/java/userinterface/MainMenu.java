package userinterface;

public class MainMenu {
    private BankMenu bankMenu;

    public MainMenu() {
        this.bankMenu = new BankMenu();
    }

    public void runMainMenu(){
        bankMenu.runBank();
    }
}
