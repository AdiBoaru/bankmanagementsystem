package userinterface;

import service.AccountService;
import service.ClientService;

import java.util.Scanner;

public class AccountMenu {
    //1
    //2
    public static void viewTransferMoneyMenu() {
        System.out.println("Transfer Money");
        System.out.println("1 - Debit Account");
        System.out.println("2 - Credit Account");
        System.out.println("3 - Person");
        System.out.println("4 - Back");
    }
    //2
    public static void actionTransferMoney() {
        Scanner scanner = new Scanner(System.in);
        viewTransferMoneyMenu();
        while (true) {
            switch (scanner.nextInt()) {
                case 1:
                    //get debit card
                    break;
                case 2:
                    //get credit account(- credit ammount)
                    break;
                case 3:
                    //person
                    break;
                case 4:
                    return;
            }
        }
    }

    public static void loggedInService(AccountService accountService, ClientService clientService) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            printLoggedInMenu();
            switch (scanner.nextInt()) {
                case 1:
                    accountService.viewPortfolioAndBalance(clientService);
                    break;
                case 2:
                    accountService.transferMoney(clientService);
                    break;
                case 3:
                    accountService.depositCash(clientService);
                    break;
                case 4:
                    accountService.viewDetailsAboutAccounts(clientService);
                    break;
                case 5:
                    accountService.createCreditAccount(clientService);
                    break;
                case 6:
                    accountService.registerAccount(clientService);
                    break;
                case 7:
                    return;
            }
        }
    }

    public static void printLoggedInMenu() {
        System.out.println("\n1 - View portfolio and balance"); //facut
        System.out.println("2 - Transfer money");
        System.out.println("3 - Deposit cash at an ATM");
        System.out.println("4 - Details about all accounts");
        System.out.println("5 - Create credit account");
        System.out.println("6 - Create Account"); //facut
        System.out.println("7 - Exit\n");
    }
}
