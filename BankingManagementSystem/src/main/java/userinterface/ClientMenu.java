package userinterface;

import config.HibernateUtils;
import repository.AccountRepository;
import repository.ClientRepository;
import service.AccountService;
import service.ClientService;

import java.util.Scanner;

public class ClientMenu {


    public int setAction() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Action:");
        return scanner.nextInt();
    }

    public void run() {
        ClientRepository clientRepository = new ClientRepository(HibernateUtils.getSessionFactory());
        ClientService clientService = new ClientService(clientRepository);
        AccountRepository accountRepository = new AccountRepository(HibernateUtils.getSessionFactory());
        AccountService accountService = new AccountService(accountRepository);

        while (true) {
            clientMenu();
            switch (setAction()) {
                case 1:
                    clientService.register();
                    break;
                case 2:
                    if (clientService.isLoggedIn()) {
                        //if logged in successful -> default account
                        AccountMenu.loggedInService(accountService, clientService);
                    }
                    break;
                case 3:
                    return;
            }
        }
    }

    public void clientMenu() {
        System.out.println("\t\tClient Menu");
        System.out.println("1 - Register Account");
        System.out.println("2 - Login");
        System.out.println("3 - Exit");
    }
}
