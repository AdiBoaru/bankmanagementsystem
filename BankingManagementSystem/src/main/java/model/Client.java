package model;


import jakarta.persistence.*;
import lombok.*;

import java.math.BigInteger;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "clients")
public class Client {
    @Id
    @Column(name = "CNP")
    private String CNP;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String pass;

    @ToString.Exclude
    @OneToMany(mappedBy = "client")
    private List<Account> accountList;

    @ManyToOne
    @JoinColumn(name = "bankId")
    private Bank bank;

    public Client(String CNP, String firstName, String lastName, String email, String username, String pass, Bank bank) {
        this.CNP = CNP;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.pass = pass;
        this.bank = bank;
    }
}
