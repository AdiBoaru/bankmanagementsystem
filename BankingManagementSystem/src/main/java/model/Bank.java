package model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

//data vs --toate
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Entity
@Table(name = "banks")
public class Bank {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bankId")
    private int bankId;
    @Column(name = "bankName")
    private String name;
    @Column(name = "address")
    private String address;

    @ToString.Exclude
    @OneToMany(mappedBy = "bank")
    private List<Client> clients;

    public Bank(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
