package model;

import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@ToString
@Entity
@Table(name = "accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int accountId;
    private String currency;
    private Double balance;
    private String type;
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "CNP")
    private Client client;

    public Account(String currency, Double balance, String type, Client client) {
        this.currency = currency;
        this.balance = balance;
        this.type = type;
        this.client = client;
    }

    public Double setBalanceWithAmount(Double balance){
        return this.balance += balance;
    }
}
