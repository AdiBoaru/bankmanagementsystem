package repository;

import exceptions.Exceptions;
import model.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;


public class AccountRepository implements CrudRepository<Account> {
    private final SessionFactory sessionFactory;

    public AccountRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Override
    public Account findById(int id) {
        try(Session session = sessionFactory.openSession()){
            return session.find(Account.class, id);
        }
    }
    public Account findByCNP(String CNP){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Account a JOIN a.client c where c.CNP = :CNP";
            return session.createQuery(hql, Account.class)
                    .setParameter("CNP", CNP)
                    .uniqueResult();
        }
    }
    public List<Account> findByCNPList(String CNP){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Account a JOIN a.client c where c.CNP = :CNP";
            return session.createQuery(hql, Account.class)
                    .setParameter("CNP", CNP)
                    .list();
        }
    }
    public Account findByCNPAndCurrency(String CNP, String currency){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Account a where a.CNP = :CNP AND a.currency = :currency";
            return session.createQuery(hql, Account.class)
                    .setParameter("CNP", CNP)
                    .setParameter("currency",currency)
                    .uniqueResult();
        }
    }
    public Account findByCreditTypeAndCNP(String CNP){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Account a JOIN a.client c WHERE a.type = 'CREDIT' AND c.CNP = :CNP";
            return session.createQuery(hql, Account.class)
                    .setParameter("CNP", CNP)
                    .uniqueResult();
        }
    }
    public Account findByDebitTypeAndCNP(String CNP){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Account a JOIN a.client c WHERE a.type = 'DEBIT' AND c.CNP = :CNP";
            return session.createQuery(hql, Account.class)
                    .setParameter("CNP", CNP)
                    .uniqueResult();
        }
    }
    @Override
    public void insert(Account account) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            try{
                transaction = session.beginTransaction();
                session.persist(account);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
        }
    }
    @Override
    public void delete(Account account) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();
                session.remove(account);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
        }
    }
    @Override
    public Account update(Account account) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            Account updatedAccount = null;
            try{
                transaction = session.beginTransaction();
                updatedAccount = session.merge(account);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
            return updatedAccount;
        }
    }
    public List<Account> findAllClientAccounts(String username){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Account a JOIN a.client c where c.username = :username";
            return session.createQuery(hql, Account.class)
                    .setParameter("username", username)
                    .list();
        }
    }
    @Override
    public List<Account> findAll() {
        try(Session session = sessionFactory.openSession()){
            String hql = "from Account";
            return session.createQuery(hql, Account.class).list();
        }
    }
}
