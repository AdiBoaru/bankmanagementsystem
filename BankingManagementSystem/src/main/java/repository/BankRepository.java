package repository;

import exceptions.Exceptions;
import model.Bank;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

//user name unic
//cnp primary key
public class BankRepository implements CrudRepository<Bank> {
    private SessionFactory sessionFactory;

    public BankRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Bank findById(int id) {
        try(Session session = sessionFactory.openSession()) {
            return session.find(Bank.class, id);
        }
    }

    public Bank findByName(String name){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Bank b where b.name = :name";
            return session.createQuery(hql, Bank.class)
                    .setParameter("name", name)
                    .uniqueResult();
        }
    }
    @Override
    public void insert(Bank bank) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            try{
                transaction = session.beginTransaction();
                session.persist(bank);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null) {
                    transaction.rollback();
                }
            }
        }
    }

    @Override
    public void delete(Bank bank) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            try{
                transaction = session.beginTransaction();
                session.remove(bank);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
        }
    }

    @Override
    public Bank update(Bank bank) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            Bank updatedBank = null;
            try{
                transaction = session.beginTransaction();
                updatedBank = session.merge(bank);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
            return updatedBank;
        }
    }

    @Override
    public List<Bank> findAll() {
        try(Session session = sessionFactory.openSession()){
            String selectAllBanksHQL = "from Bank";
            return session.createQuery(selectAllBanksHQL, Bank.class).list();
        }
    }
}
