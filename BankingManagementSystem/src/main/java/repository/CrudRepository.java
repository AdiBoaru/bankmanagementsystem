package repository;

import model.Bank;

import java.util.List;

public interface CrudRepository<T> {

    T findById(int id);


    void insert(T t);

    void delete(T t);

    T update(T t);

    List<T> findAll();

}
