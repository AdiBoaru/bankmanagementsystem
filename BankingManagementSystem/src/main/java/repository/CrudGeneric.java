package repository;

import model.Bank;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * use class for optimization
 * @param <T>
 */
public class CrudGeneric<T> implements CrudRepository<T>{
    private SessionFactory sessionFactory;

    @Override
    public T findById(int id) {
        try (Session session = sessionFactory.openSession()) {
            //   return session.find(, id);
        }
        return null;
    }


    @Override
    public void insert(T t) {

    }

    @Override
    public void delete(T t) {

    }

    @Override
    public T update(T t) {
        return null;
    }

    @Override
    public List<T> findAll() {
        return null;
    }
}
