package repository;

import exceptions.Exceptions;
import model.Bank;
import model.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ClientRepository implements CrudRepository<Client>{
    private final SessionFactory sessionFactory;

    public ClientRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    //optimizare !!
    public Client findByUsername(String username){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Client e where e.username = :username";
            return session.createQuery(hql, Client.class)
                    .setParameter("username", username)
                    .uniqueResult();
        }
    }
    public Client findByUsernamePass(String username, String pass){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Client e where e.username = :user AND e.pass = :pass";
            return session.createQuery(hql, Client.class)
                    .setParameter("user", username)
                    .setParameter("pass", pass)
                    .uniqueResult();
        }
    }

    /**
     * check the account db and verify if there is an client
     * with the current CNP
     * @return
     */
    //unuseful
    public Client findByPass(String pass){
        try(Session session = sessionFactory.openSession()){
            String hql = "from Client e where e.pass = :pass";
            return session.createQuery(hql, Client.class)
                    .setParameter("pass", pass)
                    .uniqueResult();
        }
    }

    @Override
    public Client findById(int id) {
        return null;
    }

    @Override
    public void insert(Client client) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            try{
                transaction = session.beginTransaction();
                session.persist(client);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
        }
    }

    @Override
    public void delete(Client client) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            try{
                transaction = session.beginTransaction();
                session.remove(client);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
        }
    }

    @Override
    public Client update(Client client) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = null;
            Client updatedClient = null;

            try{
                transaction = session.beginTransaction();
                updatedClient = session.merge(client);
                transaction.commit();
            }catch (Exception e){
                Exceptions.exception(e);
                if(transaction != null){
                    transaction.rollback();
                }
            }
            return updatedClient;
        }
    }

    @Override
    public List<Client> findAll() {
        try(Session session = sessionFactory.openSession()){
            String getAllClientsHQL = "from Client";
            return session.createQuery(getAllClientsHQL, Client.class).list();
        }
    }
}
